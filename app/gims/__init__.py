import os
import sys
from flask import Flask, request, Response, redirect, url_for
from werkzeug.utils import secure_filename
import tempfile


# The main.py file sets up the Flask app and handles the client requests.

#----------------- Set up the internal pathes ----------------------------------

if getattr(sys, "frozen", False):
    # Offline version
    static_folder = os.path.join(sys._MEIPASS, "static")
    SPECIES_DIR = os.path.join(sys._MEIPASS, "static/data/species_defaults/")
    # print('I am frozen!: '+static_folder)
    app = Flask(__name__, static_folder=static_folder, static_url_path="/static")
else:
    # Server version
    SPECIES_DIR = "static/data/species_defaults/"
    app = Flask(__name__)


#----------------- Client requests start here ----------------------------------

@app.route("/")
def index():
    return redirect(url_for("static", filename="index.html"))


@app.route("/parse-geometry-file", methods=["GET", "POST"])
def parse_geometry_file():
    from gims.prepare_input import get_json_structure

    if request.method == "POST":
        json_structure = get_json_structure(request.files)

        return Response(json_structure, mimetype="application/json")


@app.route("/update-structure-info", methods=["GET", "POST"])
def update_structure_info():
    from gims.prepare_input import update_json_structure

    if request.method == "POST":
        json_structure = update_json_structure(request.data)

        return Response(json_structure, mimetype="application/json")


@app.route("/generate-controlin", methods=["GET", "POST"])
def generate_controlin():
    from gims.prepare_input import get_input_files

    if request.method == "POST":
        species_dir = os.path.join(app.root_path, SPECIES_DIR)

        return get_input_files(request.data, species_dir)


@app.route("/get-download-info", methods=["POST"])
def generate_download_info():
    from gims.prepare_input import get_download_info

    if request.method == "POST":
        species_dir = os.path.join(app.root_path, SPECIES_DIR)

        return get_download_info(request.data, species_dir)


@app.route("/get-primitive-cell", methods=["POST"])
def get_primitive_cell():
    from gims.prepare_input import primtive_cell

    if request.method == "POST":
        return primtive_cell(request.data)


@app.route("/get-bz-vertices", methods=["POST"])
def get_bz_vertices():
    from gims.bz_utilities import calculate_bz_from_cell_json

    if request.method == "POST":
        return calculate_bz_from_cell_json(request.data)
