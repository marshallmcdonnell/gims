import json
from ase import Atoms
from ase.constraints import FixAtoms


class Code:
    @staticmethod
    def atomsFromDict(structDict):
        """
        Generates an ASE atoms object from the structure attached to the
        control-generator form.
        """

        cell, pbc, positions, species, constraints, c = (None,) * 6

        if "cell" in structDict:
            cell = structDict["cell"]
            pbc = True

        if "positions" in structDict:
            positions = [p["position"] for p in structDict["positions"]]
            species = [p["species"] for p in structDict["positions"]]
            magmoms = [p["initMoment"] for p in structDict["positions"]]
            constraints = [p["constraint"] for p in structDict["positions"]]

        if any(constraints):
            c = FixAtoms(mask=constraints)
        # print(positions,cell,species)

        return Atoms(
            symbols=species,
            positions=positions,
            magmoms=magmoms,
            constraint=c,
            cell=cell,
            pbc=pbc,
        )

    @staticmethod
    def atomsFromForm(formDict, isPeriodic):
        """
        Generates an ASE atoms object only from the control-generator form,
        if no structure was attached to it.
        """

        cell, pbc = None, None

        if isPeriodic:
            # Need dummy cell
            cell = [[1, 0, 0], [0, 1, 0], [0, 0, 1]]
            pbc = True

        return Atoms(symbols=formDict["species"], cell=cell, pbc=pbc)

    def getBandPathInfo(self, cell):
        header = "<h3>Band path information</h3>"
        longname = f"Bravais Lattice: {cell.get_bravais_lattice().longname} "
        description = cell.get_bravais_lattice().description()
        bandInfo = header + longname + description + "\n\n"
        self.info["DownloadInputFilesPage"]["bandInfo"] = bandInfo


class FHIaims(Code):
    def __init__(self, data, input_dir, species_dir):
        import os
        import numpy as np


        self.info = {"references": [], "DownloadInputFilesPage": {}}

        if "structure" in data:
            structure = self.atomsFromDict(data["structure"])
            if np.any(structure.get_initial_magnetic_moments()):
                data["form"]["spin"] = "collinear"
        else:
            isPeriodic = "k_grid" in data["form"]
            structure = self.atomsFromForm(data["form"], isPeriodic)

        if not "relativistic" in data["form"]:
            data["form"]["relativistic"] = "atomic_zora scalar"
        self.calcFromForm(structure, data["form"], species_dir)
        structure.calc.directory = input_dir
        self.error = None
        try:
            structure.calc.write_input(structure)
            os.remove(os.path.join(structure.calc.directory, "parameters.ase"))
            if not "structure" in data:
                os.remove(os.path.join(structure.calc.directory, "geometry.in"))
        except FileNotFoundError:
            self.error = "FileNotFoundError"

    def calcFromForm(self, struct, formDict, species_dir):
        """
        Generates the ASE calculator from the control-generator form
        """
        from ase.calculators.aims import Aims
        import os.path

        calc = Aims()
        struct.set_calculator(calc)

        for key in formDict.keys():
            if key == "basisSettings":
                calc.set(species_dir=os.path.join(species_dir, formDict[key]))
            elif key == "bandStructure":
                # print(key, formDict[key])
                self.prepare_bandinput(struct.cell, calc, density=int(formDict[key]))
            elif key == "species":
                continue
            else:
                # Only use this if 'key' is already proper FHI-aims keyword
                calc.set(**{key: formDict[key]})

    def prepare_bandinput(self, cell, calc, density=20):
        """
        Prepares the band information needed for the FHI-aims control.in file.

        Parameters:

        max_points_per_path: int
            Number of kpoints per band path
        density: int
            Number of kpoints per Angstrom. Default: 20
        """
        from ase.dft.kpoints import resolve_kpt_path_string, kpoint_convert
        import numpy as np

        self.getBandPathInfo(cell)
        bp = cell.bandpath()
        # print(cell.get_bravais_lattice())
        r_kpts = resolve_kpt_path_string(bp.path, bp.special_points)

        linesAndLabels = []
        for labels, coords in zip(*r_kpts):
            dists = coords[1:] - coords[:-1]
            lengths = [np.linalg.norm(d) for d in kpoint_convert(cell, skpts_kc=dists)]
            points = np.int_(np.round(np.asarray(lengths) * density))
            # I store it here for now. Might be needed to get global info.
            linesAndLabels.append(
                [points, labels[:-1], labels[1:], coords[:-1], coords[1:]]
            )

        bands = []
        for segs in linesAndLabels:
            for points, lstart, lend, start, end in zip(*segs):
                bands.append(
                    "band {:9.5f}{:9.5f}{:9.5f} {:9.5f}{:9.5f}{:9.5f} {:4} {:3}{:3}".format(
                        *start, *end, points, lstart, lend
                    )
                )

        calc.set(output=bands)


class Exciting(Code):
    def __init__(self, data, input_dir, species_dir):
        import os
        import shutil

        self.info = {"references": [], "DownloadInputFilesPage": {}}

        if "structure" in data:
            structure = self.atomsFromDict(data["structure"])
        else:
            isPeriodic = "nkgrid" in data["form"]
            structure = self.atomsFromForm(data["form"], isPeriodic)

        self.calcFromForm(structure, data["form"], species_dir)
        os.makedirs(os.path.dirname(input_dir))
        structure.calc.dir = input_dir
        structure.calc.write(structure)

        if not "structure" in data:
            self.insert_warning(
                "This is just a dummy structure! Please modify it!",
                "structure",
                os.path.join(input_dir, "input.xml"),
            )

        species = self.get_species(structure)
        for s in species:
            fname = s + ".xml"
            shutil.copyfile(
                os.path.join(species_dir, fname),
                os.path.join(structure.calc.dir, fname),
            )

        self.error = None

    @staticmethod
    def get_species(structure):
        species = []
        for s in structure.get_chemical_symbols():
            if s not in species:
                species.append(s)

        return species

    @staticmethod
    def insert_warning(comment, element, filename):
        import xml.etree.ElementTree as ET
        import os

        doc = ET.parse(filename)
        crystal = doc.getroot().find(element)
        crystal.insert(0, ET.Comment(comment))
        print(filename)

        doc.write(filename)
        print(os.listdir(os.path.dirname(filename)))

    def calcFromForm(self, struct, formDict, species_dir):
        """
        Generates the ASE calculator from the control-generator form
        """
        from ase.calculators.exciting import Exciting
        import os.path

        groundstate_keys = [
            "xctype",
            "rgkmax",
            "ngridk",
            "tforce",
            "epsengy",
            "epschg",
            "epsforcescf",
            "epspot",
        ]

        groundstate = {}

        paramdict = {}
        paramdict["title"] = {"text()": "{}".format(struct.get_chemical_formula())}
        for key in formDict.keys():
            if key == "ngridk":
                ks = [str(x) for x in formDict[key]]
                groundstate[key] = " ".join(ks)
            elif key in groundstate_keys:
                groundstate[key] = str(formDict[key])
            elif key == "species":
                continue
            elif key == "relax":
                paramdict["relax"] = {}
            elif key == "dos":
                paramdict["dos"] = {
                    "winddos": f"{float(formDict[key][0])} {float(formDict[key][1])}",
                    "nwdos": f"{formDict[key][2]}",
                    "lmirep": "true",
                }
            elif key == "bandStructure":
                continue
            else:
                print("key not found", key)

        paramdict["groundstate"] = groundstate
        calc = Exciting(speciespath=".", paramdict=paramdict)

        if "bandStructure" in formDict:
            self.prepare_bandinput(struct.cell, calc, density=int(formDict[key]))

        struct.set_calculator(calc)

    def prepare_bandinput(self, cell, calc, density=20):
        """
        Prepares the band information needed for the FHI-aims control.in file.

        Parameters:

        density: int
            Number of kpoints per Angstrom. Default: 20
        """
        from ase.dft.kpoints import resolve_kpt_path_string, kpoint_convert
        import numpy as np

        self.getBandPathInfo(cell)
        bp = cell.bandpath()
        r_kpts = resolve_kpt_path_string(bp.path, bp.special_points)
        length = 0
        point = []
        for labels, coords in zip(*r_kpts):
            dists = coords[1:] - coords[:-1]
            lengths = [np.linalg.norm(d) for d in kpoint_convert(cell, skpts_kc=dists)]
            length += sum(lengths)

            for label, coord in zip(labels, coords):
                point.append({"coord": "{} {} {}".format(*coord), "label": label})

        n_points = np.int_(np.round(length * density))
        bandstructure = {"plot1d": {"path": {"steps": str(n_points), "point": point}}}

        calc.paramdict["properties"] = {"bandstructure": bandstructure}
