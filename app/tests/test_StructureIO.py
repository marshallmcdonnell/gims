from gims.prepare_input import structure_auto_detection, write_json_from_atoms

class TestStructureIO:

    def test_write_json_from_atoms(self):
        from ase.io import read

        file_name = 'tests/FHIaims/test_files/geometry.in'
        with open('tests/FHIaims/test_files/structure.json') as json_file:
            json_ref = json_file.read()
        a = read(file_name)
        json_out = write_json_from_atoms(a,file_name)
        assert json_out == json_ref


    def test_structure_auto_detection_success(self):

        file_name = 'tests/FHIaims/test_files/geometry.in'
        json_out = structure_auto_detection(file_name)
        with open('tests/FHIaims/test_files/structure.json') as json_file:
            json_ref = json_file.read()
        # for key, value in json_ref[:-1]
        assert json_out == json_ref

    def test_structure_auto_detection_fail(self):

        file_name = 'tests/Exciting/test_files/input.fail'
        json_out = structure_auto_detection(file_name)

        assert json_out == "ErrorParsingGeometryFile"
