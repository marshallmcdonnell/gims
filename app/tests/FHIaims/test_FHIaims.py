import os
from gims.prepare_input import write_input_files

import json
import shutil
import tempfile

code_name = 'FHIaims'
SPECIES_DIR = 'gims/static/data/species_defaults/'
test_dir = 'tests/FHIaims/'

class TestFHIaims:

    def test_simpleCalc(self):
        # Test if input files are properly generated.
        # Import expected input files
        with open(test_dir+'test_files/data.json') as json_file:
            data = json.load(json_file)
        with open(test_dir+'test_files/geometry.in') as geo_file:
            geo_out = geo_file.readlines()
        with open(test_dir+'test_files/control.in') as control_file:
            control_out = control_file.readlines()

        # Generate actual input files
        with tempfile.TemporaryDirectory(dir='.') as tmpdir:
            print(tmpdir)
            tdir, tar_file, downloadinfo = write_input_files(code_name, data,tmpdir, SPECIES_DIR)
            print(tdir, tar_file)
            with open(os.path.join(tdir,'tar','input_files','geometry.in')) as geoIn:
                geo = geoIn.readlines()
            with open(os.path.join(tdir,'tar','input_files','control.in')) as controlIn:
                control = controlIn.readlines()
            isFile = os.path.isfile(os.path.join(tmpdir,'input_files.tar'))

        assert geo[5:] == geo_out[5:]
        assert control[5:] == control_out[5:]
        assert isFile

    def test_FileNotFound(self):
        # Test response if species file is not found.
        with open(test_dir+'test_files/data.json') as json_file:
            data = json.load(json_file)
        data['form']['basisSettings'] = 'intermeidate'

        # Generate expected input files
        with tempfile.TemporaryDirectory(dir='.') as tmpdir:
            dir, tar_file, downloadinfo = write_input_files(code_name, data,tmpdir, SPECIES_DIR)
            print(dir, tar_file)

        assert tar_file=='FileNotFoundError'

    def test_controlinOnly(self):
        # Test input preparation from control generator only (no geometry attached)
        with open(test_dir+'test_files/data.json') as json_file:
            data = json.load(json_file)
        del data['structure']
        print(data)

        with open(test_dir+'test_files/control.in') as control_file:
            control_out = control_file.readlines()

        with tempfile.TemporaryDirectory(dir='.') as tmpdir:
            dir, tar_file, downloadinfo = write_input_files(code_name, data,tmpdir, SPECIES_DIR)
            print(dir, tar_file)
            with open(os.path.join(dir,'tar','input_files','control.in')) as controlIn:
                control = controlIn.readlines()

            isTarFile = os.path.isfile(os.path.join(tmpdir,'input_files.tar'))
            isGeometry = os.path.isfile(os.path.join(dir,'tar','input_files','geometry.in'))

        assert control[5:] == control_out[5:]
        assert isTarFile
        assert not isGeometry




#Test = TestFHIaims()
#Test.test_simpleCalc()
