#===============================================================================
# FHI-aims file: temp/tar/input_files/control.in
# Created using the Atomic Simulation Environment (ASE)
# Mon Jan 20 18:41:32 2020
#===============================================================================
xc                                 pbesol
k_grid                             4 4 4
relax_geometry trm                 5e-3
relax_unit_cell                    full
sc_accuracy_forces                 1e-3
compute_analytical_stress          .true.
output dos                         -100 10 1000 0.1
sc_accuracy_etot                   1e-3
sc_accuracy_rho                    1e-3
sc_accuracy_eev                    1e-3
relativistic                       atomic_zora scalar
#===============================================================================

################################################################################
#
#  FHI-aims code project
#  VB, Fritz-Haber Institut, 2009
#
#  Suggested "light" defaults for Cd atom (to be pasted into control.in file)
#  Be sure to double-check any results obtained with these settings for post-processing,
#  e.g., with the "tight" defaults and larger basis sets.
#
################################################################################
  species        Cd
#     global species definitions
    nucleus             48
    mass                112.411
#
    l_hartree           4
#
    cut_pot             3.5  1.5  1.0
    basis_dep_cutoff    1e-4
#
    radial_base         62 5.0
    radial_multiplier   1
    angular_grids specified
      division   0.5937   50
      division   1.0282  110
      division   1.3769  194
      division   1.7301  302
#      division   2.2341  434
#      division   2.2741  590
#      division   2.3152  770
#      division   2.3574  974
#      division   2.9077 1202
#      outer_grid  974
      outer_grid  302
################################################################################
#
#  Definition of "minimal" basis
#
################################################################################
#     valence basis states
    valence      5  s   2.
    valence      4  p   6.
    valence      4  d  10.
#     ion occupancy
    ion_occ     5  s   1.
    ion_occ     4  p   6.
    ion_occ     4  d   9.
################################################################################
#
#  Suggested additional basis functions. For production calculations, 
#  uncomment them one after another (the most important basis functions are
#  listed first).
#
#  Constructed for dimers: 2.15 A, 2.50 A, 3.10 A, 4.00 A, 5.00 A
#
################################################################################
#  "First tier" - max. impr. -224.92  meV, min. impr. -5.81 meV
     hydro 2 p 1.6
     hydro 4 f 7
     hydro 3 s 2.8
     hydro 3 p 5.2
#     hydro 5 g 10.0
     hydro 3 d 3.8
#  "Second tier" - max. impr. -2.57  meV, min. impr. -0.38 meV
#     hydro 4 f 17.6
#     hydro 6 h 13.6
#     hydro 3 p 7
#     hydro 5 s 17.6
#     hydro 3 d 3.4
#  "Third tier" - max. impr. -0.37 meV, min. impr. -0.09 meV
#     hydro 3 p 2.4
#     hydro 4 f 6.4
#     hydro 4 s 4
#     hydro 5 f 15.6
#     hydro 5 g 10.8
#     hydro 2 p 6.4
#     hydro 6 h 12.8
#     hydro 5 d 9.0
#  Further functions: -0.05 meV and below
#     hydro 5 d 5.8  
################################################################################
#
#  FHI-aims code project
#  VB, Fritz-Haber Institut, 2009
#
#  Suggested "light" defaults for Sr atom (to be pasted into control.in file)
#  Be sure to double-check any results obtained with these settings for post-processing,
#  e.g., with the "tight" defaults and larger basis sets.
#
################################################################################
  species          Sr
#     global species definitions
    nucleus        38
    mass           87.62
#
    l_hartree      4
#
    cut_pot        4.0  1.5  1.0
    basis_dep_cutoff    1e-4
#
    radial_base    57  5.5
    radial_multiplier  1
    angular_grids specified
      division   0.6981  110
      division   0.9394  194
      division   1.1230  302
#      division   1.2482  434
#      division   1.3391  590
#      division   1.4365  770
#      division   7.0005  974
#      outer_grid  974
      outer_grid  302
################################################################################
#
#  Definition of "minimal" basis
#
################################################################################
#     valence basis states
    valence      5  s   2.
    valence      4  p   6.
    valence      3  d  10.
#     ion occupancy
    ion_occ      5  s   1.
    ion_occ      4  p   6.
    ion_occ      3  d  10.
################################################################################
#
#  Suggested additional basis functions. For production calculations, 
#  uncomment them one after another (the most important basis functions are
#  listed first).
#
#  Constructed for dimers: 2.75, 3.50, 4.40, 5.00 A
#
################################################################################
#  "First tier" - improvements: -289.57 meV to -14.02 meV
     ionic 4 d auto
     ionic 5 p auto
#     hydro 4 f 5.6
     ionic 5 s auto
#  "Second tier" - improvements: -4.95 meV to -0.45 meV
#     hydro 5 g 7.4
#     hydro 4 d 4.4
#     hydro 3 p 3.3
#     hydro 6 h 10.4
#     hydro 5 s 4.9
#     hydro 5 f 13.2
#  "Third tier" - improvements: -0.38 meV to -0.11 meV
#     hydro 6 p 4.8
#     hydro 5 f 6
#     hydro 2 p 1.2
#     hydro 1 s 0.55
#     hydro 5 d 3.6   
#  "Fourth tier" - improvements: -0.12 meV and lower.
#     hydro 5 p 5.2
#     hydro 4 f 14.8
#     hydro 5 g 7.6
#     hydro 4 p 4.5
#     hydro 5 d 5.4
#     hydro 6 s 6.8   
################################################################################
#
#  FHI-aims code project
#  VB, Fritz-Haber Institut, 2009
#
#  Suggested "light" defaults for Ta atom (to be pasted into control.in file)
#  Be sure to double-check any results obtained with these settings for post-processing,
#  e.g., with the "tight" defaults and larger basis sets.
#
################################################################################
  species          Ta
#     global species definitions
    nucleus        73
    mass           180.94788
#
    l_hartree      4
#
    cut_pot        3.5  1.5  1.0
    basis_dep_cutoff    1e-4
#
    radial_base    71  5.0
    radial_multiplier  1
    angular_grids specified
      division   0.3792   50
      division   1.0232  110
      division   1.3396  194
      division   1.5892  302
#      division   1.8380  434
#      division   2.1374  590
#      division   2.2049  770
#      division   2.2755  974
#      division   2.8291 1202
#      outer_grid  974
      outer_grid  302
################################################################################
#
#  Definition of "minimal" basis
#
################################################################################
#     valence basis states
    valence      6  s   2.
    valence      5  p   6.
    valence      5  d   3.
    valence      4  f  14.
#     ion occupancy
    ion_occ      6  s   1.
    ion_occ      5  p   6.
    ion_occ      5  d   2.
    ion_occ      4  f  14.
################################################################################
#
#  Suggested additional basis functions. For production calculations, 
#  uncomment them one after another (the most important basis functions are
#  listed first).
#
#  Constructed for dimers: 1.85, 2.12, 2.625, 3.25, 4.50 AA
#
################################################################################
#
#  "First tier" - improvements: -461.64 meV to -31.84 meV
     hydro 4 f 7
     hydro 4 d 5.6
     ionic 6 p auto
#     hydro 5 g 11.6
     ionic 6 s auto
#  "Second tier" - improvements: -26.62 meV to -1.73 meV
#     ionic 5 d auto
#     hydro 6 h 15.2
#     hydro 4 f 9.2
#     hydro 5 g 17.6
#     hydro 4 p 4.7
#     hydro 1 s 0.5  
#  "Third tier" - max. impr. -6.67 meV, min. impr. -0.18 meV
#     hydro 5 d 8  
#     hydro 6 h 20
#     hydro 5 g 38
#     hydro 5 f 8.4
#     hydro 2 p 1.3
#     hydro 5 s 9.4  
#  "Fourth tier" - max. impr. -0.76 meV, min. impr. -0.13 meV
#     hydro 5 g 10.8
#     hydro 6 p 14.8
#     hydro 6 h 20.4
#     hydro 5 f 21.2
#     hydro 4 p 11.6
#     hydro 4 f 3.9
#     hydro 6 d 13.6
#     hydro 4 s 4.6
