from gims.prepare_input import get_download_info, get_input_files
from gims import app
import json


json_byte = b'{"code":"FHIaims","structure":{"cell":[[7.55,0,0],[0,7.55,0],[0,0,7.55]],"positions":[{"position":[0,3.775,3.775],"species":"Cd","initMoment":0,"constraint":false},{"position":[5.6625,5.6625,1.8875],"species":"Sr","initMoment":0,"constraint":false},{"position":[3.775,3.775,3.775],"species":"Ta","initMoment":0,"constraint":false},{"position":[0,0,0],"species":"Cd","initMoment":0,"constraint":false},{"position":[5.6625,1.8875,5.6625],"species":"Sr","initMoment":0,"constraint":false},{"position":[3.775,0,0],"species":"Ta","initMoment":0,"constraint":false},{"position":[3.775,3.775,0],"species":"Cd","initMoment":0,"constraint":false},{"position":[1.8875,5.6625,5.6625],"species":"Sr","initMoment":0,"constraint":false},{"position":[0,3.775,0],"species":"Ta","initMoment":0,"constraint":false},{"position":[3.775,0,3.775],"species":"Cd","initMoment":0,"constraint":false},{"position":[1.8875,1.8875,1.8875],"species":"Sr","initMoment":0,"constraint":false},{"position":[0,0,3.775],"species":"Ta","initMoment":0,"constraint":false}]},"form":{"species":["Cd","Sr","Ta"],"xc":"pz-lda","k_grid":[4,4,4],"basisSettings":"light","bandStructure":40}}'

expected_answer = {'references': [], 'DownloadInputFilesPage': {'bandInfo': '<h3>Band path information</h3>Bravais Lattice: primitive cubic CUB(a=7.55)\n  Variant name: CUB\n  Special point names: GXRM\n  Default path: GXMGRX,MR\n\n  Special point coordinates:\n    G   0.0000  0.0000  0.0000\n    X   0.0000  0.5000  0.0000\n    R   0.5000  0.5000  0.5000\n    M   0.5000  0.5000  0.0000\n\n\n'}}

species_dir = 'gims/static/data/species_defaults'

class TestDownload:

    def test_generateDownloadInfo(self):
        answer = json.loads(get_download_info(json_byte,species_dir))
        for key, value in answer.items():
            assert expected_answer[key] == value

    # def test_generateControlin(self):
    #     with app.app_context():
    #         answer = get_input_files(json_byte,species_dir)
    #         print(answer)
    #         assert True == True
