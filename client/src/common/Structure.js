/**
 * @author Iker Hurtado
 *
 * @fileoverview File holding the Structure class
 */

import * as mathlib from './math.js'
// import * as math from '../../lib/math.js'


/**
 * A class modeling a atomic structure, either a periodic or non-periodic system
 * It's usually gotten from a file
 */
export default class Structure {

  constructor() {
    this.latVectors // Array of arrays (3x3)
    this.atoms = []
    this.fileSource = ""
    this.structureInfo = {}
    this.id
  }


  /**
   * Reset the structure
   */
  reset(){
    this.latVectors = undefined
    this.atoms = []
    this.fileSource = undefined
    this.structureInfo = undefined
  }


  /**
   * Returns if it's a periodic system or not
   * @return {boolean}
   */
  isAPeriodicSystem(){
    //console.log('isAPeriodicSystem',this.latVectors !== undefined, this.latVectors)
    return this.latVectors !== undefined
  }


  /**
   * Returns true if any of the lattice vectors is not zero vector
   * @return {boolean}
   */
  areLatticeVectorsComplete(){
    return (this.latVectors) && ( !isZero(this.latVectors[0]) && !isZero(this.latVectors[1]) && !isZero(this.latVectors[2]) )

    function isZero(a){
      return a[0] === 0 && a[1] === 0 && a[2] === 0
    }
  }


  /**
   * Removes the lattice vectors becaming a non-periodic system
   */
  removeLatticeVectors(){
    this.latVectors = undefined
  }


  /**
   * Updates the lattice vectors and scales the atoms positions if specified
   * @param {Array<float, float>} vectors
   * @param {boolean} scaleAtomsPosition This indicates if the atoms positions must be scaled
   */
  updateLatticeVectors(vectors, scaleAtomsPosition){ // Set or transform a periodic system
    const fractPositions = []
    if (scaleAtomsPosition)
      this.atoms.forEach( atom => {
        fractPositions.push(this.getFractionalCoordinates(atom.position))
      })

    this.latVectors = vectors

    if (scaleAtomsPosition)
      for (let i = 0; i < this.atoms.length; i++) {
        this.atoms[i].position = this.getCartesianCoordinates(fractPositions[i])
      }
  }


  /**
   * Creates a new atom object (it's not added to the structure)
   * @param {Array<float, float>} cartCoords Atom cartesian coordiantes
   * @param {string} species
   * @param {int} moment Atom initial moment
   * @param {boolean} constraint
   * @return {object}
   */
  createAtom(cartCoords, species, moment, constraint){

      return {
        'position': cartCoords,
        'species': species,
        'initMoment': moment,
        'constraint': constraint,

        /**** Nicer solution
        'fractPosition': undefined // For non periodic

        'fractPosition': function(){
          const fractCoors = math.lusolve(math.transpose(latVectors), cartCoords)
          return math.transpose(fractCoors)[0]
        }*/
      }

  }


  /**
   * Creates and add a new atom object
   * @param {Array<float, float>} cartCoords Atom cartesian coordiantes
   * @param {string} species
   * @param {boolean} isFract It indicates if the position coordinates are fractional
   * @param {int} moment Atom initial moment
   * @param {boolean} constraint
   */
  addAtomData(pos, species, isFract = false, moment = 0, constraint = false){
    let cCoors = (isFract ? this.getCartesianCoordinates(pos) : pos)
    this.atoms.push(this.createAtom(cCoors, species,moment,constraint))
    //console.log('Structure.addAtomData', this.atoms)
  }


  /**
   * Creates an undefined atom.
   * It returns the new atom position in the structure
   * @return {int}
   */
  addUndefinedAtom(){
    this.addAtomData([0, 0, 0], undefined)
    return this.atoms.length-1
  }


  /**
   * Updates a structure atom species
   * @param {int} atomIndex Index of the atom  to be updated
   * @param {string} species
   */
  updateAtomSpecies(atomIndex, species){
    this.atoms[atomIndex].species = species
  }


  /**
   * Updates a structure atom initialization moment
   * @param {int} atomIndex Index of the atom  to be updated
   * @param {int} initMoment
   */
  updateAtomInitMoment(atomIndex, initMoment){
    this.atoms[atomIndex].initMoment = initMoment
  }


  /**
   * Updates a structure atom constrain
   * @param {int} atomIndex Index of the atom  to be updated
   * @param {int} constraint
   */
  updateAtomConstraint(atomIndex, constraint){
    this.atoms[atomIndex].constraint = constraint
  }


  /**
   * Updates a structure atom position
   * @param {int} atomIndex Index of the atom  to be updated
   * @param {Array<float>} values
   * @param {boolean} isFract If the values are fractional
   */
  updateAtomPosition(atomIndex, values, isFract = false){
    this.updateAtomCoorValue(atomIndex, 0, values[0], isFract)
    this.updateAtomCoorValue(atomIndex, 1, values[1], isFract)
    this.updateAtomCoorValue(atomIndex, 2, values[2], isFract)
  }


  /**
   * Updates a coordinate of a structure atom
   * If the
   * @param {int} atomIndex Index of the atom to be updated
   * @param {int} coorIndex
   * @param {float} value
   * @param {boolean} isFract If the value is given in fractional
   */
  updateAtomCoorValue(atomIndex, coorIndex, value, isFract = false){
    let atom = this.atoms[atomIndex]
    if (atom === undefined){
      atom = this.createAtom([0, 0, 0], undefined)
      this.atoms[atomIndex] = atom
    }
    if (isFract){
      // console.log('POSSIBLE ERROR HERE')
      atom.position[coorIndex] = this.getCartesianCoordinates(value)[coorIndex]
    }else
      atom.position[coorIndex] = value
  }


  /**
   * Sets the initialization moment to the last atom
   * @param {int} moment
   */
  setLastAtomInitMoment(moment){
    //console.log(this.atomPositions.length-1, moment)
    this.atoms[this.atoms.length-1].initMoment = moment;
  }


  /**
   * Returns the cartesian coordinates corresponding to the
   * fractional coordinates passed as the argument.
   * It behaves as a static method
   * @param {Array<float>} fractCoors
   * @return {Array<float>}
   */
  getCartesianCoordinates(fractCoors){
    let cartPos = []
    cartPos = mathlib.multiplyScalar(this.latVectors[0], fractCoors[0])
    cartPos = mathlib.addArrays( cartPos, mathlib.multiplyScalar(this.latVectors[1], fractCoors[1]) ) // absPos.add(basisVectors[1].clone().multiplyScalar(position.y));
    cartPos = mathlib.addArrays( cartPos, mathlib.multiplyScalar(this.latVectors[2], fractCoors[2]) ) // absPos.add(basisVectors[2].clone().multiplyScalar(position.z));
    //console.log('getCartesianCoordinates', cartPos)
    return cartPos;
  }


  /**
   * Returns a structure atom fractional coordinates
   * @return {int} i Atom index
   * @return {Array<float>}
   */
  getAtomFractPosition(i){
    return this.getFractionalCoordinates(this.atoms[i].position)
  }


  /**
   * Returns the fractional coordinates corresponding to the
   * cartesian coordinates passed as the argument.
   * It behaves as a static method
   * @param {Array<float>} cartCoors
   * @return {Array<float>}
   */
  getFractionalCoordinates(cartCoors){
      //console.log('basisMatrix', basisMatrix)
    if (this.isAPeriodicSystem()){
      const fractCoors = mathlib.solve33(this.latVectors, cartCoors)
      return fractCoors // transform the column format to a regular array
    }else
      return undefined
  }

}
