/**
 * @author Iker Hurtado
 *
 * @fileoverview Form is an UI component that intends to make easy the setup
 * of fully-featured forms in the application
 */

import UIComponent from './UIComponent.js'

/**
 * UI component to set up forms
 */
export default class Form extends UIComponent{

  /**
   * @param {string} className - Specific class name for the form instance
   */
  constructor(className){ //
    super('div', '.Form '+className)

    this.setHTML(`<div class="user-msg"><div>`)

    this.addElement('div',`<div class="info"><div>`)
    this.fieldMap = new Map()
    this.initValues = new Map()
    this.validationInfo = new Map()
    this.explicitInclusionInfo = new Map()
    this.datatype = new Map()
    this.info = this.getElement('.info')

    // User message area. It can be used from outside and disabled
    // The fields validation messages are shown here
    this.userMsg = this.getElement('.user-msg')
  }


  /**
   * It returns the html input of the named field.
   * This could be an array of inputs for multi-fields
   * @param {string} name - Field name (identificator)
   * @return {HTMLElement} Html input of the named field
   */
  getField(name){
    return this.fieldMap.get(name)
  }


  /**
   * It returns the html element containing the input field(s)
   * @param {string} name - Field name (identificator)
   * @return {HTMLElement} Html element containing the field(s)
   */
  getFieldBox(name){
    let field = this.fieldMap.get(name)
    if (field) // it could be undefined, no value field
      if (Array.isArray(field))
        return this.fieldMap.get(name)[0].parentElement
      else return this.fieldMap.get(name).parentElement
  }


  /**
   * It returns the field label html element
   * @param {string} name - Field name (identificator)
   * @return {HTMLElement} Field label html element
   */
  getFieldLabelElement(name){
    return this.getElement('.'+name+'-form-field label')
  }


  /**
   * It returns the number of fields
   * @return {int} Number of fields
   */
  getFieldNum(){
    return this.fieldMap.size
  }


  /**
   * It adds a new section to the form.
   * The form can be divided in sections holding the fields
   * @param {string} name - Field name (identificator)
   * @return {HTMLElement} The section element
   */
  addSection(name){
    let section = this.addElement('div')
    section.className = 'section-title'
    section.innerHTML = name
    return section
  }

  addInfo(text) {
    this.info.innerText = text
  }


  /**
   * It adds a new field to the form.
   *
   * @param {string} name - Field name (identificator)
   * @param {string} text - Field name shown to the user
   * @param {string} type - String that determines the type (input-text,
   * input-range, select, etc) and the number of sub-fields (multifield)
   * @param {string} value - Default value for the field
   * @param {boolean} required - If the value is mandatory or not
   * @param {boolean} explicitInclusion - If the field has to be explicitly
   * indicated (by a checkbox on the left) to be included as a regular field
   * @return {HTMLElement or Array<HTMLElement>}
   * The new field or set of fields added
   */
  addField(name, text, type, value, datatype, group=0, required=true, explicitInclusion=false,units=''){

    if (value) this.initValues.set(name, value)
    else value = ''
    // console.log(name)
    this.validationInfo.set(name, required)
    this.explicitInclusionInfo.set(name, explicitInclusion)
    this.datatype.set(name,datatype)
    const fieldWrapper = this.addElement('div')
    fieldWrapper.className = name+'-form-field'

    // If this has to be explicitly included
    let checkboxHTML = '<div class="explicit-inclusion-box"></div>'
    if (explicitInclusion)
      checkboxHTML = `<div class="explicit-inclusion-box"><input type="${explicitInclusion}" name="${group}" class="${name}-explicitInclusion-${explicitInclusion}" ondblclick="this.checked=false;"></div>`
    fieldWrapper.innerHTML = checkboxHTML+' <label>'+text+`</label> <div class="value-box"></div><div class="units-box"></div>`
    const valueInputBox = fieldWrapper.querySelector('.value-box')
    const unitsBox = fieldWrapper.querySelector('.units-box')

    // Exceptional case
    if (type === 'no-input'){
      this.fieldMap.set(name, undefined)
      return
    }

    // Follow the regular case

    // Check if the field is multiple (num > 1), it is composed by several form fields
    let num = 1
    let subfieldNames
    if (type.includes(':')){
      let a = type.split(':')
      type = a[0]
      num = parseInt(a[1])
      if (a.length === 3) subfieldNames = a[2].split(',')
    }

    // Create the fields
    let isInput =  false
    // console.log(type);
    if (type.startsWith('input')) isInput = true
    let field
    const setOfFields = []
    for (let i = 0; i < num; i++) {

      field = document.createElement(isInput ? 'input' : type)
      if (isInput) field.type = type.substring(type.indexOf('-')+1)

      if (subfieldNames){ // case with named subfields
        let subfieldsBox = document.createElement('div')
        subfieldsBox.className = 'named-subfield'
        let label = document.createElement('span')
        label.innerHTML = subfieldNames[i]
        subfieldsBox.appendChild(label)
        subfieldsBox.appendChild(field)
        valueInputBox.appendChild(subfieldsBox)

        let subfieldsUnitsBox = document.createElement('div')
        subfieldsUnitsBox.className = 'named-subfield-units'
        let unitText = document.createElement('div')
        unitText.innerHTML = units? units[i] : ''
        subfieldsUnitsBox.appendChild(unitText)
        unitsBox.appendChild(subfieldsUnitsBox)
      }else{ // case no-named subfields -> In one row
        field.style.width = (100/num)+'%'
        valueInputBox.appendChild(field)
        unitsBox.innerHTML = units
      }
      if (num > 1) setOfFields.push(field)
    }

    this.fieldMap.set(name, (num > 1 ? setOfFields : field ) )

    // Set the value or values
    if (Array.isArray(value)){ // select or input-range
      if (isInput && field.type === 'range'){
        field.min =  value[0]; field.max =  value[1];
        field.step =  'any'/*value[2]; */;field.value =  value[2];// field.value =  value[0]
      }else{ // select - several values (only one field supported for now)
        value.forEach( v => {
          let option = document.createElement('option')
          if (v.includes(':')){
            option.text = v.split(':')[0]
            option.value = v.split(':').slice(1).join(':')
          }else
            option.text = v
          field.add(option)
        })
      }
    }else{
      if (num > 1)
        for (let i = 0; i < num; i++) setOfFields[i].value = value
      else field.value = value
    }
    return (num > 1 ? setOfFields : field )
  }


  /**
   * It adds a button to the form (not included by default, it can be controled
   * from outside)
   * @param {string} label - Button label
   * @return {HTMLElement} The new button
   */
  addButton(label){
    let div = document.createElement('div')
    div.className = 'Form-button-box'
    this.button = document.createElement('button')
    this.button.innerHTML = label
    div.appendChild(this.button)
    this.e.appendChild(div)
    return this.button
  }


  /**
   * It validates that the required fields are filled
   * @return {boolean} If the validation was good or not
   */
  validate(){
    let valid = true
    this.validationInfo.forEach( (required, name) => {
      // console.log('validate', name, required)
      const field = this.fieldMap.get(name)
      if (field !== undefined)
        if (Array.isArray(field)){ // multiple fields
          let values = []
          field.forEach( f => {
            if (f.value !== '')
              values.push(f.value)
          })
          // console.log(values,required)
          // console.log(values.length === field.length, (!required && values.length===0));
          if (values.length === field.length || (!required && values.length===0)){
            field.forEach( f => {
              f.classList.remove('markedField')
            })
          } else {
            field.forEach( f => {
              if (f.value === '')
                f.classList.add('markedField')
            })
            // console.log('Turns invalid',field)
            valid = false
          }
        }
        else{ // simple field
          if (required && field.value === ''){
            field.classList.add('markedField')
            // console.log('Turns invalid',field)

            valid = false
          }else field.classList.remove('markedField')
        }

    })
    if (!valid) this.userMsg.innerHTML = 'There are some fields required'+
      ' to generate the control.in file not filled'
    else this.userMsg.innerHTML = ''
    // console.log('valid',valid)
    return valid
  }


  /**
   * It marks or unmarks a field normally in the validation process
   * @param {string} fieldName - Field name (identificator)
   * @param {int} i - Subfield index in the field set
   * @param {boolean} unmark - If the operation is the opposite: unmark the
   * field instead of marking it
   */
  markField(fieldName, i, unmark = false){
    let field = (i !== undefined ?
      this.fieldMap.get(fieldName)[i] : this.fieldMap.get(fieldName))
    //log('markField',fieldName, field)
    if (unmark) field.classList.remove('markedField')
    else field.classList.add('markedField')
  }


  /**
   * It sets a user message
   * @param {string} msg - Message for the user
   */
  setUserMsg(msg){
    this.userMsg.innerHTML = msg
  }


  /**
   * It disables the user message area. Enabled by default
   */
  disableUserMsg(){
    this.userMsg.style.display = 'none'
  }


  /**
   * It returns an object with the pairs: field name-value
   * @return {object} Object containing the fields name-value pairs
   */
  getFieldsNameValue(){
    let result = {}
    // console.log(this.datatype)
    this.fieldMap.forEach( (field, name) => {
      // console.log(field, name)
      let myDataType = this.datatype.get(name)
      // console.log(name, this.datatype.get(name),'.'+name+'-explicitInclusion-checkbox')
      // If the field doesn't need explicit inclusion or does it and it's checked
      let myEI = this.explicitInclusionInfo.get(name)
      if (!myEI
        || this.getElement('.'+name+`-explicitInclusion-${myEI}`).checked){

        if (field === undefined){
          if (myDataType === 'boolean')
            result[name] = true
          else if (myDataType === 'none')
            result[name] = ''
        }else if (Array.isArray(field)){ // multiple fields
          let values = []
          for (let i = 0; i < field.length; i++) {
            let dtype = myDataType[i]
            // console.log(field[i],myDataType[i])
            values.push(parseValue2(field[i].value,dtype))
          }
          result[name] = values
        }else{
          if (field.type === 'checkbox') result[name] = field.checked
          else {
            // console.log(field.value);
            if (field.value.includes('{')) {
              let setOfValues = JSON.parse(field.value)
              // console.log(setOfValues);
              for (let key in setOfValues){
                // console.log(key,setOfValues[key]);
                result[key] = setOfValues[key]
              }
            } else
              result[name] = parseValue2(field.value,myDataType)
          }
        }
      }
    })
    return result

    function parseValue2(v,dtype){
      if (dtype==='float')
        return parseFloat(v)
      else if (dtype==='integer')
        return parseInt(v)
      else return v
    }
  }


  /**
   * It resets the fields values to their inital values
   */
  resetFieldsValues(){ // NO well tested
    this.fieldMap.forEach( (field, name) => {
      //log('resetFieldsValues', field, name)
      let value = this.initValues.get(name)
      if (Array.isArray(value)){ // select or input-range
        if (field.type === 'range') field.value =  value[2]
        else field.option[0].selected = true // select case
      }else{
        if (Array.isArray(field)) // Field with subfields
          for (let i = 0; i < field.length-1; i++) field[i].value = value[i]
        else field.value = value
      }
    })
  }

}
