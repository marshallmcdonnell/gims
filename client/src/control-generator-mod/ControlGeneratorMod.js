/**
 * @author Iker Hurtado
 *
 * @fileoverview File holding the ControlGeneratorMod class and module
 */

import UIComponent from '../common/UIComponent.js'
import Form from '../common/Form.js'
import {Conf} from '../Conf.js'
import * as util from '../common/util.js'
import * as UserMsgBox from '../common/UserMsgBox.js'
import * as Tooltip from '../common/Tooltip.js'
import * as AppState from '../State.js'
import downloadIcon from '../../img/download-icon.png'
import collapsibleIcon from '../../img/collapsible-icon.png'
import AllFields from './AllFields.js'

/**
 * ControlGenerator application module UI component
 */
export default class ControlGeneratorMod extends UIComponent{

	constructor() {
    super('div', '#ControlGeneratorMod')
    this.setHTML(`
      <h1> Control Generator </h1>

      <div class="content">

        <div class="form-accordion" > </div>

        <div class="download-inputfiles-button">
          <a class="download-inputfiles-link" download="input_files.tar">
            <img src="${downloadIcon}" />Generate and download input file(s)
          </a>
        </div>

      </div>
    `)

    this.allFields = new AllFields()  // All available fields
    this.fields // The effective fields (data) for this instance
    this.forms // There is a form per accordion section
    this.workflow


    this.formAccordion = new Accordion('control-gen-accordion')
    this.getElement('.form-accordion').appendChild(this.formAccordion.e)

    this.button = this.getElement('.download-inputfiles-button')
    this.downloadLink = this.getElement('.download-inputfiles-link')
    this.button.addEventListener('click', e => {
      e.preventDefault() // This prevent the regular link behavior (the link is empty at that time)
      // because we want it after the request callback when it is linked to the tar file
      this.requestInputFiles( tarFiles => {
        this.downloadLink.href =
          window.URL.createObjectURL( new Blob([tarFiles], {type: 'application/x-tar'} ))
        // The first time the user clicks on the link the tar file is got from the server,
        // the link href is still empty so no file to download is shown to the user
        // therefore a new click event is created and issued on the link to get the download offer
        this.downloadLink.dispatchEvent(new MouseEvent('click'))
      })
    })

    this.standalone = true // standalone module (not inside a workflow)
  }


  /**
   * Initilization method
   * @param  {string} confString Configuration string listing the fields (comma separated)
   * forming the control generator
   */
  init(workflow){
    this.buildAccordion(workflow)
  } // init()


  /**
   * Builds the accordion UI component
   */
  buildAccordion(workflow){
    // reset
    // console.log(workflow);
    this.workflow = workflow
    workflow = workflow ? workflow : 'default'
    console.log('buildAccordion',workflow)
    this.fields = {}
    this.forms = []
    this.formAccordion.reset()
    Tooltip.setRelPosition('right', 200)

    let myCode = AppState.getCode()
    let myGroups = this.allFields[myCode]

    myGroups.forEach((group,i) => {
      let form = new Form('control-gen-form')
      form.disableUserMsg()
      if (group.info) form.addInfo(group.info)
      for (let prop in group.fields){
        let myField = group.fields[prop]
        // console.log(group.fields[prop]);
        if ( myField.workflow === workflow || myField.workflow ==='default'){
          // console.log(group.fields[prop]);
          this.fields[prop] = myField
          let data = myField
          form.addField(
						prop, data.text, data.inputType, data.value, data.dataType, i,
						data.required, data.explicitInclusion, data.units
					)
          let labelElement = form.getFieldLabelElement(prop)
          let text = group.fields[prop].explanation
          if (text){
            labelElement.setAttribute('data-explanation', text)
            Tooltip.addTargetElement(labelElement)
          }
        }
      }
      if (form.getFieldNum() > 0){ // The empty sections are not shown
        this.formAccordion.setSection(group.label, form.e)
        this.forms.push(form)
      }
    })
    if (workflow === 'BandStructure') this.formAccordion.foldSection(1)
  }


  /**
   * Provides an input structure. The species filed is filled up
   * Optional use for the instance. Used in instances inside workflows.
   * @param {Structure} structure
   */
  setStructure(structure){
		console.log('Calling set structure');
    this.standalone = false
    this.button.style.display = 'none'

    this.periodicSystem = structure.isAPeriodicSystem()

    // If the structure is non-periodic the kgrid field is hidden and not validated
    this.checkIfKGrid()

    // It's assumed the especies field is present
    let species = []
    structure.atoms.forEach( atom => {
      if (!species.includes(atom.species))
        species.push(atom.species)
    })
    const field = this.forms[0].fieldMap.get('species')
    field.value = species.join(' ')
    field.disabled = true

    this.structure = { cell: structure.latVectors, positions: structure.atoms}
    // console.log('requestControlInFile structure', this.structure)
  }

  checkIfKGrid(){
		// console.log('Calling checkIfKGrid()');
    if (this.fields.kgrid !== undefined && !this.standalone){
      let kgridFieldBox = this.forms[0].getFieldBox('kgrid')
      kgridFieldBox.parentElement.style.display = (this.periodicSystem ?  '' : 'none')
      kgridFieldBox.style.display = (this.periodicSystem ?  'block' : 'none')
        // Also un-display the label

      // If the field is visible it has to be validated
      this.forms[0].validationInfo.set('kgrid', this.periodicSystem)
    }
  }


  /**
   * Updates the component for a code change (application level event)
   * @param  {string} code
   */
  updateForCode(code){
    // log('ControlGen updateForCode', code, this.forms[0].getField('species').value)
    const speciesFieldValue = this.forms[0].getField('species').value
    // console.log('this.',this.)
    this.buildAccordion(this.workflow)
    this.checkIfKGrid()
    this.forms[0].getField('species').value = speciesFieldValue

    this.downloadLink.href = undefined
  }


  /**
   * Updates the component for a setting change (application level event)
   * @param  {object} newSettings
   */
  updateSettings(newSettings){
  }


  /**
   * Goes through the field getting the values entered and builds a json object
   * representing a server request to get a input file
   * @return {object}
   */
  getInputFilesRequestJson(){

    /* Convenience lines for development
    this.forms[0].getField('species').value = 'H'
    if (AppState.isExcitingCode()) this.forms[0].getField('xctype').value = 'EXX' // Exciting
    else this.forms[0].getField('xc').value = 'hf' // FHIaims
    this.forms[0].getField('kgrid')[0].value = 9;  this.forms[0].getField('kgrid')[1].value = 9;  this.forms[0].getField('kgrid')[2].value = 9
    if (AppState.isExcitingCode())  this.forms[0].getField('rgkmax').value = 7 // Exciting
    else this.forms[0].getField('basisSettings').value = 'light' // FHIaims
  */

    // If any of the forms is not validated the function returns
    for (let i = 0; i < this.forms.length; i++)
      if (!this.forms[i].validate()){
        UserMsgBox.setError('There are some fields required to generate the control.in file not filled')
        return false
      }

    let namesValues = {}
    this.forms.forEach( (form,i) => {
      console.log(form);
      namesValues[i] = form.getFieldsNameValue()
    })
    console.log('From the form', namesValues)

    // KGRID FIELD
    if (this.fields.kgrid !== undefined){

      if ((this.standalone && !namesValues[0].kgrid.every((value) => Number.isNaN(value)) )
        || this.periodicSystem){
        // console.log('namesValues.kgrid', namesValues[0].kgrid)
        namesValues[0].kgrid.forEach( (value, i) => {
          if (Number.isNaN(value)){
            this.forms[0].markField('kgrid', i)
            UserMsgBox.setError('Some kgrid position is not a valid number')
            return false
          }
        })
      } else // it's in a workflow and it's NOT a periodic system
        delete namesValues[0].kgrid
    }
    // SPECIES FIELD. It's assumed the especies field is present
		let allSpecies = namesValues[0].species
		let species = []
		if (allSpecies.includes(',')) {
			allSpecies.split(',').forEach(s => species.push(s.trim()))
		} else {
			allSpecies.split(' ').forEach(s => species.push(s.trim()))
		}
    for (let i = 0; i < species.length; i++)
      if (Conf.getSpeciesAtomicNumber(species[i]) === 0){
        this.forms[0].markField('species')
        UserMsgBox.setError('Some species is not valid: ' + species[i]) //this.form.userMsg.innerHTML = 'Some species is not valid'
        return
      }
    namesValues[0].species = species

    // A new object is built with the field flags as object keys
    let flagsValues = {}
    let myCode = AppState.getCode()
    let myGroups = this.allFields[myCode]
    for (let i in namesValues){
      for (let prop in namesValues[i]){
        // console.log(i, prop, myGroups[i]['fields'][prop]['flag']);

        if (myGroups[i]['fields'][prop] && myGroups[i]['fields'][prop]['flag']) {

						flagsValues[myGroups[i]['fields'][prop]['flag']] = namesValues[i][prop]
        } else
        // if (FIELDS_INFO[prop].flagAims || FIELDS_INFO[prop].flagExc){
        //   if (AppState.isExcitingCode() )
        //     flagsValues[FIELDS_INFO[prop].flagExc] = namesValues[prop]
        //   else flagsValues[FIELDS_INFO[prop].flagAims] = namesValues[prop]
        // }else
          flagsValues[prop] = namesValues[i][prop]
      }
    }  //    log('flags', flagsValues, namesValues)

    let inputFilesRequestJson = { code: AppState.getCode(), structure: this.structure, form: flagsValues }
    log('inputFilesRequestJson', inputFilesRequestJson)
    return inputFilesRequestJson


  }


    /**
     * Fetches a server request asking for a tar file packaging a set of input files
     * That is passed in a listener
     * @param {function} listener
     */
    async requestInputFiles(listener){
    if (!this.getInputFilesRequestJson()) return
    let response = await fetch('/generate-controlin', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json;charset=utf-8' },
      body: JSON.stringify(this.getInputFilesRequestJson())
    })

    if (response.ok){ // File or server controled errors
      // It's needed to get the text because of the FileNotFoundError checking
      let tarFiles = await response.text() //await response.arrayBuffer()
      if (tarFiles === 'FileNotFoundError'){
        UserMsgBox.setError('Species defaults not found. Check your species. Not all species are defined for <i>intermediate</i>.')
      }else
        listener(tarFiles)//, requestJson)
    }else{ // Unknown server ERROR
      UserMsgBox.setError('Unknown server ERROR ')
    }
  }

  async requestDownloadInfo(listener){
    //log('DownloadInputFilesPage requestInputFiles', requestJson)
    if (!this.getInputFilesRequestJson()) return
    let response = await fetch('/get-download-info', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json;charset=utf-8' },
      body: JSON.stringify(this.getInputFilesRequestJson())
    })

    if (response.ok){ // File or server controled errors
      // It's needed to get the text because of the FileNotFoundError checking
      let downloadInfo = await response.text() //await response.arrayBuffer()
      console.log(downloadInfo)
      listener(downloadInfo)//, requestJson)
    }else{ // Unknown server ERROR
      UserMsgBox.setError('Unknown server ERROR ')
    }
  }


}



/**
 * Accordion UI component used to fold/unfold the different sections in the form
 */
class Accordion extends UIComponent{

  /**
   * @param  {string} className Component class name
   */
  constructor(className){ //
    super('div', '.Accordion'+ (className === undefined ? '' : ' '+className ))

    this.ROTATE_STYLE = 'transform: rotate(180deg)'
    this.sectionHeaders = []
    this.sectionContents = []
  }


  /**
   * Resets the component. Remove everything
   */
  reset(){
    this.sectionHeaders = []
    this.sectionContents = []
    this.setHTML('')
  }


  /**
   * Sets a section in accordeon
   * @param {string} name
   * @param {HTMLElement} element
   */
  setSection(name, element){
    let sectionHeader = this.addElement('div', name+` <img src="${collapsibleIcon}" /> `)
    let icon = sectionHeader.querySelector('img')
    sectionHeader.className = 'sectionHeader'
    this.sectionHeaders.push(sectionHeader)

    sectionHeader.addEventListener('click', e => {
      this.foldSection(this.sectionHeaders.indexOf(sectionHeader))
    })

    let sectionContent = this.addElement('div')
    sectionContent.className = 'sectionContent'
    sectionContent.appendChild(element)
    this.sectionContents.push(sectionContent)
    if (this.sectionContents.length > 1){
      sectionContent.style.display = 'none'
    }else
      icon.style = this.ROTATE_STYLE
    //this.e.appendChild(element)
  }


  /**
   * Folds or unfolds the section depending on its current state
   * @param  {int} section index
   */
  foldSection(n){ //
    let content = this.sectionContents[n]
    let folded = (content.style.display === 'none')
    content.style.display = (folded ? 'block' : 'none')
    this.sectionHeaders[n].querySelector('img').style = (folded ? this.ROTATE_STYLE : '')
  }

}
