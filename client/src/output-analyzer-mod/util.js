/**
 * @author Iker Hurtado
 *
 * @fileoverview Utilty file for the Output Analyzer module
 */


// import * as THREE from "../../lib/three.js"
import * as THREE from "three/build/three.min.js"
import OutputAims from './OutputAims.js'
import OutputExciting from './OutputExciting.js'


// Global utility functions
window.showElement = (element, show = true) => { element.style.display = ( show ? '' : 'none' ) }
window.log = (...msg) => { console.log(...msg) }
window.error = (...msg) => { console.error(...msg) }

window.onerror = function(message, url, line, col, error) {
  UserMsgBox.setError(`${message}\n At ${line}:${col} of ${url}`)
}


/**
 * Returns an FHIaims or Exciting output instance based
 * on the output files imported
 * @param  {Array<object>} filesData
 * @return {Output}
 */
export function getOutputInstance(filesData){
  let outputAims = new OutputAims()
  outputAims.parseFiles(filesData)
  if (outputAims.isFilledUp()) return outputAims

  let outputExciting = new OutputExciting()
  outputExciting.parseFiles(filesData)
  if (outputExciting.isFilledUp())  return outputExciting
}


export function geoTextFromStructure(structure) {
  let text = ''
  let latticeVectors = structure.latVectors
  if (latticeVectors) {
    latticeVectors.forEach(v => {
      text += 'lattice_vector '+ v.join(' ') +'\n'
    })
  }
  structure.atoms.forEach(atom => {
    text += `atom ${atom.position.join(' ')} ${atom.species}\n`
  })
  return text
}


/**
 * Joins a list of text lines in a text and returns it
 * @param  {Array<string>} lines
 * @return {string}
 */
export function lineArrayToText(lines){
   let controlInText = ''
   lines.forEach( line => {
     controlInText += line+'\n'
   })
   return controlInText
}


/**
 * Returns the volume of a parallelepiped,
 * defined by three vectors
 * @param  {Array<Array>} vectors
 * @return {float}
 */
export function getParallelepipedVolume(vectors) { // units =Amstrongs
   let v0 = new THREE.Vector3().fromArray(vectors[0])
   let v1 = new THREE.Vector3().fromArray(vectors[1])
   let v2 = new THREE.Vector3().fromArray(vectors[2])
   return Math.abs(v0.cross(v1).dot(v2))
}


/**
 * Returns HTML code wrapping and layouting output quantities
 * @param  {Map<string,object>}
 * @param  {boolean}
 * @return {string}
 */
export function getHtmlRows(quantities, valuesAsNumbers = true){
  let html = ''
  quantities.forEach( (quantity, label) => {
    if (label === 'download-link'){
      html += `<tr> <td><a class="f-geometry-download-link" download="geometry.in"
               href="${quantity}" >Download final geometry </a></td></tr>`
    }
    else
      html += `<tr> <td>${label}</td> <td>${valuesAsNumbers ? quantity.toFixed(5) : quantity}</td> </tr>`
  })
  return html
}
