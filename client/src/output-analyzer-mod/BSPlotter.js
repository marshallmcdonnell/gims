/**
 * @author Iker Hurtado
 *
 * @fileoverview File holding the BSPlotter graph
 */

 import InteractiveGraphBase from './InteractiveGraphBase.js' 

/**
 * Indicates the proportion (gap) of the graph width occupated by the lacking bands
 * @type {float}
 */
const LACKING_BANDS_GAP = 0.1

/**
 * Implements a Band Structure plotter.
 */
export default class BSPlotter{

  /**
   * @param  {object} margin Object like {left: , right: , top: , bottom: }
   * @param  {string} xAxisLabel X axis label
   * @param  {string} yAxisLabel Y axis label
   * @param  {string} spin1Color Spin 1 plot color
   * @param  {string} spin1Color Spin 2 plot color
   */
  constructor(margins, xAxisLabel, yAxisLabel, spin1Color = '#000000', spin2Color = '#000000') {
    this.baseGraph = new InteractiveGraphBase(margins)
    this.xAxisLabel = xAxisLabel
    this.yAxisLabel = yAxisLabel
    this.spin1Color = spin1Color
    this.spin2Color = spin2Color
    this.factor = 1
  }

  
  /**
   * Creates and attachs the graph canvas inside a HTML layout element. 
   * Sets the width and height of the canvas, and optionaly the 
   * canvas resolution can be doubled
   * @param  {HTMLElement} element
   * @param  {int} width
   * @param  {int} height
   * @param  {boolean} doubleRes
   */
  attach(element, width, height, doubleRes = false){
    this.baseGraph.attach(element, width, height, doubleRes);//super.attach(element, width, height);
  }


  /**
   * Returns the hightest and lowest values of the bands
   * @param  {object} bandStructData
   * @return {Array<float>}
   */
  getTopAndLowestPoints(bandStructData){
    //log('getTopAndLowestPoints', bandStructData)
    let bandMax = -10000;
    let bandMin = 10000;
    for (let i = 0; i < bandStructData.length; i++) { // Per segment
      // spin1 - per band energy loop
      for (let j = 0; j < bandStructData[i].band_energies[0].length; j++) {
        let tempValue= Math.max.apply(null, bandStructData[i].band_energies[0]/*spin1*/[j]/*first_band*/);
        if (tempValue > bandMax) bandMax= tempValue;
        tempValue= Math.min.apply(null, bandStructData[i].band_energies[0]/*spin1*/[j]/*first_band*/);
        if (tempValue < bandMin) bandMin= tempValue;
      }
      //log('segment', i, bandMin, bandMax)
      if (bandStructData[i].band_energies.length === 2)
        // spin2 - per band energy loop
        for (let j = 0; j < bandStructData[i].band_energies[1].length; j++) { // Per segment
          let tempValue= Math.max.apply(null, bandStructData[i].band_energies[1]/*spin1*/[j]/*first_band*/);
          if (tempValue > bandMax) bandMax= tempValue;
          tempValue= Math.min.apply(null, bandStructData[i].band_energies[1]/*spin1*/[j]/*first_band*/);
          if (tempValue < bandMin) bandMin= tempValue;
        }
    }
    return [bandMin/this.factor, bandMax/this.factor];
  }


  /**
   * Draws a k-point label on the X axis
   * @param  {int} x
   * @param  {string} label
   */
  drawKPointLabel(x, label){
    this.baseGraph.setXAxisTickText(x, label)
  }

  /**
   * Sets the band structure data to be plotted. 
   * Also the top and bottom X values can be passed  
   * @param {object} data
   * @param {int} eAxisMin
   * @param {int} eAxisMax
   */
  setBandStructureData(bandStructData, eAxisMin = -100, eAxisMax = 100){
    //log('bandStructData', bandStructData)
    // Gather all the points per band (divided by spin) crossing the segments
    this.bandsDataSpin1= []  // [segment][band][kpoint]
    this.bandsDataSpin2= []
    this.segmentLimitsX = []

    let topAndLowestPoints = this.getTopAndLowestPoints(bandStructData)
    let minEnergyVal = topAndLowestPoints[0]
    let maxEnergyVal = topAndLowestPoints[1]

    this.baseGraph.setAxisRangeAndLabels(this.xAxisLabel, false, 0, 1, 
      this.yAxisLabel, eAxisMin, eAxisMax, minEnergyVal, maxEnergyVal, 5)

    // Calculates de distance
    let totalDistance= 0
    let prevBandNum = 0
    for (let k = 0; k < bandStructData.length; k++) {
      if ((prevBandNum+1) !== bandStructData[k].segment_num){
        bandStructData[k].lacking_prev_segments = true
        totalDistance+= LACKING_BANDS_GAP
      } 
      prevBandNum = bandStructData[k].segment_num

      let kPoints= bandStructData[k].band_k_points
      totalDistance+= kPointDistance(kPoints,kPoints.length-1)
    }
    let currentDistance= 0
    let prevLastLabel = null
    let dataOverflow = false

    for (let k = 0; k < bandStructData.length; k++) { // For every  segment

      let segment = bandStructData[k]
      let kPoints = segment.band_k_points
      let labels = segment.band_segm_labels

      let energiesSpin1= segment.band_energies[0]
      let energiesSpin2= segment.band_energies[1]
      this.bandsDataSpin1.push([])  // Add a new array per segment
      this.bandsDataSpin2.push([])

      let segmentDistance= kPointDistance(kPoints,kPoints.length-1);

      if (segment.lacking_prev_segments){
        this.segmentLimitsX.push(currentDistance/totalDistance);
        //this.segmentLimitsGaps.push(currentDistance/totalDistance)
        currentDistance += LACKING_BANDS_GAP
      } 

      // keeping the segment limits (x coordenate) for after painting
      this.segmentLimitsX.push(currentDistance/totalDistance)

      if (labels !== undefined){
        // Set k-points labels
        if (prevLastLabel !== null && prevLastLabel !== labels[0]){

          if ((segment.segment_num-1) === bandStructData[k-1].segment_num){ // following segments
            this.drawKPointLabel(currentDistance/totalDistance, getSymbol(prevLastLabel)+'|'+getSymbol(labels[0]))
          }else{ // misssing segment(s) between
            this.drawKPointLabel((currentDistance-LACKING_BANDS_GAP)/totalDistance, getSymbol(prevLastLabel))
            this.drawKPointLabel(currentDistance/totalDistance, getSymbol(labels[0]))
          }
        }else
          this.drawKPointLabel(currentDistance/totalDistance,getSymbol(labels[0]))
        // The last label
        if (k === bandStructData.length -1)
          this.drawKPointLabel(1, getSymbol(labels[1]));

        prevLastLabel = labels[1];
      }

      for (let i = 0; i < kPoints.length; i++) { // loop k-points (points in every band)

        let tempDistance= (currentDistance + kPointDistance(kPoints, i))/totalDistance;
          // All bands spin1
        if (energiesSpin1[i] !== undefined)
          for (let j = 0; j < energiesSpin1[i].length; j++) {  // loop bands

            if (i === 0) this.bandsDataSpin1[k][j] = [];//if (k === 0 && i === 0) this.bandsDataSpin1[j] = [];
            let currentY = energiesSpin1[i][j]/this.factor;
            this.bandsDataSpin1[k][j].push({x: tempDistance, y: currentY});  
            if (!dataOverflow && currentY > 10000)  dataOverflow = true;
          }
        // All bands spin2        log('energiesSpin2[i]', i, energiesSpin2[i])
        if (energiesSpin2[i] !== undefined){
          for (let j = 0; j < energiesSpin2[i].length; j++) {
            if (i === 0) this.bandsDataSpin2[k][j] = [];
            let currentY = energiesSpin2[i][j]/this.factor;
            this.bandsDataSpin2[k][j].push({x: tempDistance, y: currentY});
            if (!dataOverflow && currentY > 10000)  dataOverflow = true;
          }
        }
        //console.log("K PPPPPP Ponint: "+i+' DIS: '+tempDistance, this.bandsDataSpin1[k]);
      }

      currentDistance+= segmentDistance;
    }

    // Drawing part

    if (dataOverflow) 
      throw 'Plotter Data Overflow: Probably the energy data is not in correct units'

    else{
      this.baseGraph.addLineGroup('segmentsLimits', 
        { defaultColor: '#999', strokeWidth: 0.4, dash: true} )

      this.segmentLimitsX.forEach(x => {
          this.baseGraph.setLineData([x, minEnergyVal, x, maxEnergyVal], 'segmentsLimits')
       })

      this.baseGraph.addLineGroup('spin1', {defaultColor: this.spin1Color})
      for (let i = 0; i < this.bandsDataSpin1.length; i++){ // loop the segments
        for (let j = 0; j < this.bandsDataSpin1[i].length; j++) { // loop the bands
          let points = []
          for (let k = 0; k < this.bandsDataSpin1[i][j].length; k++) { // loop the kpoints
            points.push(this.bandsDataSpin1[i][j][k].x)
            points.push(this.bandsDataSpin1[i][j][k].y)
          }
          this.baseGraph.setLineData(points, 'spin1')
        }  
      }

      if (this.bandsDataSpin2.length > 0){
        this.baseGraph.addLineGroup('spin2', {defaultColor: this.spin2Color})
        for (let i = 0; i < this.bandsDataSpin2.length; i++) // loop the segments
          for (let j = 0; j < this.bandsDataSpin2[i].length; j++) { // loop the bands
            let points = []
            for (let k = 0; k < this.bandsDataSpin2[i][j].length; k++) { // loop the kpoints
              points.push(this.bandsDataSpin2[i][j][k].x)
              points.push(this.bandsDataSpin2[i][j][k].y)
            }
            this.baseGraph.setLineData(points, 'spin2')
          }
      }

      this.baseGraph.draw()
    } 
  }

  /**
   * Sets a listening function before every repainting
   * @param {function} listener
   */
  setRepaintListener(listener) {
    this.baseGraph.repaintListener = listener
  }

}


/**
 * Return s the distance between first k-point and given for the position 
 * @param  {Array<float>} kPoints
 * @param  {int} position
 * @return {float}
 */
function kPointDistance(kPoints, position){
  let p0= kPoints[0];
  let p1= kPoints[position];
  let deltaX= p1[0] - p0[0];
  let deltaY= p1[1] - p0[1];
  let deltaZ= p1[2] - p0[2];
  return Math.sqrt(deltaX*deltaX + deltaY*deltaY + deltaZ*deltaZ);
}


/**
 * Returns the k-point label. Transforms several possible Gamma labels 
 * in the symbol
 * @param  {string} label
 * @return {string}
 */
function getSymbol(label){
  if (label === 'Gamma' || label === 'GAMMA' || label === 'G') return 'Γ';
  else return label;
}

