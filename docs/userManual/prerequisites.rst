Prerequisites
=============

All main features of GIMS are roughly tested for the following Browsers: Google Chrome, Safari (MacOS), Firefox.
However, some specific features, which we may have not tested explicitly, may not work for all browsers. GIMS is developed to primarily work for the Chrome and Chromium browser. Please consider to use them, if you experience any problems for your favorite browser. Feel free to report any issues in the GIMS repository: https://gitlab.com/gims-developers/gims
