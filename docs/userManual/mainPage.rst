Main Page
=========

The figure below shows the main page of the GIMS application. An example can be found here:
https://gims.ms1p.org .

.. image:: ../images/appsMainPage.png



1. **App Selector:** Here, you can select the individual apps. They are described in detail in the subsequent chapters. Hovering over the question mark displays a short description below the app icon. It is further subdivided into two parts: a) workflow apps (*Simple Calculation* and *Band Structure*) and Elemental apps (*Structure Builder*, *Control Generator*, and *Output Analyzer*).

2. **Code Selection:** By clicking on the corresponding code icon the underlying behavior of the apps is adapted to the selected code.

3. **Settings:** The settings menu enables to change display properties, such as the number of shown floating point digits.

4. **GIMS Button:** The GIMS button is the *home button*, that is, clicking on this icon always returns to the apps main page.

5. **Desktop application:** Click on this icon to get a description how to download and install the GIMS offline executable.

6. **Feedback and User Manual:** This *Feedback* icon re-directs to the issue tracker of the GIMS repository. You can leave suggestions, improvements, and bug reports, here. The *User Manual* icon redirects you to the current web site.
